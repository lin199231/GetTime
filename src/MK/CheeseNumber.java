package MK;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheeseNumber {
	public static String toAllNumic(String msg) {
		StringBuffer strb = new StringBuffer(msg);
		String temp;
		Pattern Zero = Pattern.compile("零[一二三四五六七八九]");// 零数
		Pattern Single = Pattern.compile("[一二三四五六七八九]");// 一位数
		Pattern Double = Pattern.compile("十[一二三四五六七八九]?");// 两位数
		Pattern Twenty = Pattern.compile("[一二三四五六七八九]十");// 二十这样的两位数
		Pattern Triple = Pattern.compile("[一二三四五六七八九]十[一二三四五六七八九]");// 三位数
		Matcher MC = null;// 匹配器
		MC = Triple.matcher(msg);
		while (MC.find()) {
			temp = MC.group();
			strb.replace(MC.start(), MC.start() + 3, toNumic(temp.charAt(0))
					+ "" + toNumic(temp.charAt(2)));
			msg = strb.toString();
			MC = Triple.matcher(msg);
		}
		MC = Twenty.matcher(msg);
		while (MC.find()) {
			temp = MC.group();
			strb.replace(MC.start(), MC.start() + 2, toNumic(temp.charAt(0))
					+ "0");
			msg = strb.toString();
		}
		MC = Double.matcher(msg);
		while (MC.find()) {
			temp = MC.group();
			if (temp.length() == 2)
				strb.replace(MC.start(), MC.start() + 2,
						"1" + toNumic(temp.charAt(1)));
			else
				strb.replace(MC.start(), MC.start() + 2, "10");
		}
		MC = Zero.matcher(msg);
		while (MC.find()) {
			temp = MC.group();
			strb.replace(MC.start(), MC.start() + 2,
					"0" + toNumic(temp.charAt(1)));
		}
		MC = Single.matcher(msg);
		while (MC.find()) {
			temp = MC.group();
			strb.replace(MC.start(), MC.start() + 1, toNumic(temp.charAt(0))
					+ "");
		}
		return msg = strb.toString();
	}

	private static char toNumic(char c) {
		switch (c) {
		case '一':
			return '1';
		case '二':
			return '2';
		case '三':
			return '3';
		case '四':
			return '4';
		case '五':
			return '5';
		case '六':
			return '6';
		case '七':
			return '7';
		case '八':
			return '8';
		case '九':
			return '9';
		default:
			return '0';
		}
	}
}

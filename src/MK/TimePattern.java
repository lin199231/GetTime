package MK;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//关于所有用到的正则表达式的类
public class TimePattern {
	private String text;// 正则表达式的文本
	private static Pattern Date = Pattern
			.compile("((\\d{4}|\\d{2})[年\\.\\\\/-])?((\\d{1,2}[月\\.\\\\/-]\\d{1,2}[日号]?)|(\\d{1,2}[日号]))");
	// xx/xxxx年x-xx月x-xx日
	private static Pattern Year = Pattern.compile("(\\d{4}|\\d{2})[年\\.\\\\/-]");// xx-xxxx年
	private static Pattern Month = Pattern.compile("\\d{1,2}[月\\.\\\\/-]");// x-xx月
	private static Pattern Day = Pattern.compile("\\d{1,2}[日号]?");// x-xx日
	private static Pattern NextWeek = Pattern
			.compile("下(星期|礼拜|周)[一二三四五六日天1-7]");// 下星期x
	private static Pattern Week = Pattern.compile("(星期|礼拜|周)[一二三四五六日天1-7]");// 星期x
	private static Pattern TS = Pattern.compile("[AaPp].?[Mm].?");// am/pm
	private static Pattern Time = Pattern
			.compile("\\d{1,2}(([点时](半|[123一二三]刻|\\d{1,2}分|\\d{1,2}))|([：:]\\d{1,2})|[点])");// 精确时间
	private static Pattern DayWord = Pattern
			.compile("(今天|明天|后天|大后天|今晚|半夜|凌晨|明早|明晚)(上午|中午|下午|早上|清晨|早晨|晚上|傍晚)?");// x-xx日
	private static Pattern TimeWord = Pattern
			.compile("((((((\\d{4}|\\d{2})[年\\.\\\\/-])?((\\d{1,2}[月\\.\\\\/-]\\d{1,2}[日号]?)|(\\d{1,2}[日号])))|"
					+ "((星期|礼拜|周)[一二三四五六日天1-7])|"
					+ "(今天|明天|后天|大后天|今晚|半夜|凌晨|明早|明晚))(上午|中午|下午|早上|清晨|早晨|晚上|傍晚)?"
					+ "(\\d{1,2}(([点时](半|[123一二三]刻|\\d{1,2}分|\\d{1,2}))|([：:]\\d{1,2})|[点])))|"// 多个时间词
					+ "(((((\\d{4}|\\d{2})[年\\.\\\\/-])?((\\d{1,2}[月\\.\\\\/-]\\d{1,2}[日号]?)|(\\d{1,2}[日号])))|"
					+ "((星期|礼拜|周)[一二三四五六日天1-7])|"
					+ "(今天|明天|后天|大后天|今晚|半夜|凌晨|明早|明晚))(上午|中午|下午|早上|清晨|早晨|晚上|傍晚)?|"
					+ "(\\d{1,2}(([点时](半|[123一二三]刻|\\d{1,2}分|\\d{1,2}))|([：:]\\d{1,2})|[点]))))");// 至少一个时间词
	private static Pattern SpiltTimeWord = Pattern
			.compile("(((((\\d{4}|\\d{2})[年\\.\\\\/-])?((\\d{1,2}[月\\.\\\\/-]\\d{1,2}[日号]?)|(\\d{1,2}[日号])))|"
					+ "((星期|礼拜|周)[一二三四五六日天1-7])|"
					+ "(今天|明天|后天|大后天|今晚|半夜|凌晨|明早|明晚))(上午|中午|下午|早上|清晨|早晨|晚上|傍晚)?(.+?)"
					+ "(\\d{1,2}(([点时](半|[123一二三]刻|\\d{1,2}分|\\d{1,2}))|([：:]\\d{1,2})|[点])))");
	
	public TimePattern() {
	}

	public TimePattern(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Matcher getDateMatcher() {
		return Date.matcher(text);
	}

	public Matcher getYearMatcher() {
		return Year.matcher(text);
	}

	public Matcher getMonthMatcher() {
		return Month.matcher(text);
	}

	public Matcher getDayMatcher() {
		return Day.matcher(text);
	}

	public Matcher getWeekMatcher() {
		return Week.matcher(text);
	}

	public Matcher getNextWeekMatcher() {
		return NextWeek.matcher(text);
	}

	public Matcher getTSMatcher() {
		return TS.matcher(text);
	}

	public Matcher getTimeMatcher() {
		return Time.matcher(text);
	}

	public Matcher getDayWordMatcher() {
		return DayWord.matcher(text);
	}

	public Matcher getTimeWordMatcher() {
		return TimeWord.matcher(text);
	}

	public Matcher getSpiltTimeWordMatcher(){
		return SpiltTimeWord.matcher(text);
	}
	public static Matcher getDateMatcher(String text) {
		return Date.matcher(text);
	}

	public static Matcher getYearMatcher(String text) {
		return Year.matcher(text);
	}

	public static Matcher getMonthMatcher(String text) {
		return Month.matcher(text);
	}

	public static Matcher getDayMatcher(String text) {
		return Day.matcher(text);
	}

	public static Matcher getWeekMatcher(String text) {
		return Week.matcher(text);
	}

	public static Matcher getNextWeekMatcher(String text) {
		return NextWeek.matcher(text);
	}

	public static Matcher getTSMatcher(String text) {
		return TS.matcher(text);
	}

	public static Matcher getTimeMatcher(String text) {
		return Time.matcher(text);
	}

	public static Matcher getDayWordMatcher(String text) {
		return DayWord.matcher(text);
	}

	public static Matcher getTimeWordMatcher(String text) {
		return TimeWord.matcher(text);
	}
	
	public static Matcher getSpiltTimeWordMatcher(String text) {
		return SpiltTimeWord.matcher(text);
	}
}

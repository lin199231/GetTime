package MK;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.*;

////////////////////////////////////////////////////
//String[] text为分词后的结果，对于地点的判断和选择 对其进行操作
//String Location为记录地点的字符串

public class GetTime {
	public GetTime() {
		this("", true);
	}

	public GetTime(String msg) {
		this(msg, true);
	}

	public GetTime(String msg, boolean chineseNumber) {
		this.chineseNumber = chineseNumber;
		NoDateMsg = msg;
		Msg = msg;
	}

	public boolean hasTime() {
		return hasTime;
	}

	private static boolean hasTimePattern(String msg) {
		Matcher date = null;// 匹配器
		Matcher week = null;// 匹配器
		Matcher time = null;// 匹配器
		Matcher word = null;// 匹配器
		date = TimePattern.getDateMatcher(msg);
		week = TimePattern.getWeekMatcher(msg);
		word = TimePattern.getDayWordMatcher(msg);
		time = TimePattern.getTimeMatcher(msg);
		if (!date.find() && !week.find() && !time.find() && !word.find())
			return false;
		else
			return true;
	}

	public void setMsg(String msg) {
		// SegmentationByHash seg=new SegmentationByHash();
		// text = seg.getWords(msg);
		Msg = msg;
	}

	public String getMsg() {
		return Msg;
	}

	public String[] getText() {// 用于测试使用
		return text;
	}

	public String getNoDateMsg() {
		dropAllDate();
		return NoDateMsg;
	}

	public void analyse(String mark) {
		if (chineseNumber) {
			Msg = CheeseNumber.toAllNumic(Msg);
		}
		if (hasTimePattern(Msg)) {
			hasTime = true;
			text = Msg.split(mark);
			timeList = new ArrayList<Calendar>();
			for (int i = 0; i < text.length; i++)
				if (hasTimePattern(text[i]))
					timeList.add(PhaseShiefTime(text[i]));
		}
	}

	public void analyse() {
		if (chineseNumber) {
			Msg = CheeseNumber.toAllNumic(Msg);
		}
		if (hasTimePattern(Msg)) {
			hasTime = true;
			autoSplit();
			timeList = new ArrayList<Calendar>();
			for (int i = 0; i < text.length; i++)
				if (hasTimePattern(text[i]))
					timeList.add(PhaseShiefTime(text[i]));
		}
	}

	private void autoSplit() {
		Matcher mc = null;// 匹配器
		ArrayList<String> split = new ArrayList<String>();
		mc = TimePattern.getTimeWordMatcher(Msg);
		Matcher spiltMatcher = TimePattern.getSpiltTimeWordMatcher(Msg);
		int start = 0;
		int end = 0;
		while (true) {
			if (spiltMatcher.find(end)) {
				if (spiltMatcher.start() != 0) {
					split.add(Msg.substring(start, spiltMatcher.start()));
					start = spiltMatcher.start();
					end = spiltMatcher.end();
				} else {
					end = spiltMatcher.end();
					if (!spiltMatcher.find(end))
						break;
				}
				System.out.println("spilt:" + start + "-" + end);
			} else if (mc.find(end)) {
				System.out.println("Whole");
				if (mc.start() != 0) {
					split.add(Msg.substring(start, mc.start()));
					start = mc.start();
					end = mc.end();
				} else {
					end = mc.end();
					if (!mc.find(end))
						break;
				}
				System.out.println("whole:" + start + "-" + end);
			} else {
				break;
			}
		}
		split.add(Msg.substring(start, Msg.length()));
		text = new String[split.size()];
		for (int i = 0; i < text.length; i++) {
			text[i] = split.get(i);
			System.out.println(text[i]);
		}
	}

	public Calendar getTime(int i) {// 返回日历对象
		if (chineseNumber) {
			Msg = CheeseNumber.toAllNumic(Msg);
		}
		// tp=new TimePattern(Msg);
		// PhaseShiefTime();
		if (hasTime)
			return timeList.get(i);
		else
			return null;
	}

	public Calendar getFirstTime() {// 返回日历对象
		if (chineseNumber) {
			Msg = CheeseNumber.toAllNumic(Msg);
		}
		// PhaseShiefTime();
		if (hasTime)
			return timeList.get(0);
		else
			return null;
	}

	public Calendar getLastTime() {// 返回日历对象

		// PhaseShiefTime();
		if (hasTime)
			return timeList.get(timeList.size());
		else
			return null;
	}

	public String getTest() {
		String context = "";
		if (timeList != null) {
			for (int i = 0; i < timeList.size(); i++) {
				context += timeList.get(i).get(Calendar.YEAR) + "年"
						+ (timeList.get(i).get(Calendar.MONTH) + 1) + "月"
						+ timeList.get(i).get(Calendar.DATE) + "日"
						+ timeList.get(i).get(Calendar.HOUR_OF_DAY) + ":"
						+ timeList.get(i).get(Calendar.MINUTE);
				context += "\n";
			}
		}
		return context;

	}

	private Calendar PhaseShiefTime(String msg) {// 用于解析出字符串中的时间和地点信息
		tp = new TimePattern(msg);
		Calendar time = Calendar.getInstance();// 获取系统当前时间
		// time.set(Calendar.AM_PM, Calendar.AM);
		time.set(Calendar.SECOND, 0);
		time.setFirstDayOfWeek(Calendar.MONDAY);
		// 正则表达式确定
		boolean setWeek = false, setSureDate = false;
		int start;
		int year, month, day, hour, minute;
		year = time.get(Calendar.YEAR);
		month = time.get(Calendar.MONTH);
		day = time.get(Calendar.DATE);
		hour = time.get(Calendar.HOUR_OF_DAY);
		minute = time.get(Calendar.MINUTE);

		// 明天 后天 大后天 晚上 分词后进行校正
		// 提取xxxx年xx月xx日/号
		Matcher MC = null;// 匹配器

		MC = tp.getDateMatcher();
		if (MC.find()) {
			start = MC.start();// 从匹配位置开始分解匹配
			MC = tp.getYearMatcher();
			if (MC.find(start)) {
				start = MC.end();// 从年份之后匹配月份
				if (MC.group().length() == 5)
					year = Integer.valueOf(MC.group().substring(0, 4));
				else
					year = Integer.valueOf(MC.group().substring(0, 2));
			}// 提取年份
			MC = tp.getMonthMatcher();
			if (MC.find(start)) {
				start = MC.end();// 从月份之后匹配日期
				month = Integer.valueOf(MC.group().substring(0,
						MC.group().length() - 1)) - 1;
			}// 提取月份
			MC = tp.getDayMatcher();
			if (MC.find(start)) {
				if (MC.group().charAt(MC.group().length() - 1) == '日'
						|| MC.group().charAt(MC.group().length() - 1) == '号')
					day = Integer.valueOf(MC.group().substring(0,
							MC.group().length() - 1));
				else
					day = Integer.valueOf(MC.group().substring(0,
							MC.group().length()));
				setSureDate = true;
			}// 提取日期
		}
		// 提取下星期\星期x
		MC = tp.getNextWeekMatcher();
		if (MC.find()) {

			// year = time.get(Calendar.YEAR);
			// month = time.get(Calendar.MONTH);
			// day = time.get(Calendar.DATE);//超级疑问为什么这里不运行 时间就不能改为下星期
			switch (MC.group().charAt(MC.group().length() - 1)) {
			case '一':
			case '1':
				time.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
				break;
			case '二':
			case '2':
				time.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
				break;
			case '三':
			case '3':
				time.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
				break;
			case '四':
			case '4':
				time.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
				break;
			case '五':
			case '5':
				time.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
				break;
			case '六':
			case '6':
				time.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
				break;
			case '天':
			case '日':
			case '7':
				time.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
				break;
			}
			setWeek = true;
			setSureDate = true;
			time.set(Calendar.WEEK_OF_YEAR, time.get(Calendar.WEEK_OF_YEAR) + 1);
			year = time.get(Calendar.YEAR);
			month = time.get(Calendar.MONTH);
			day = time.get(Calendar.DATE);
		} else {
			MC = tp.getWeekMatcher();
			if (MC.find()) {
				switch (MC.group().charAt(MC.group().length() - 1)) {
				case '一':
				case '1':
					time.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
					break;
				case '二':
				case '2':
					time.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
					break;
				case '三':
				case '3':
					time.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
					break;
				case '四':
				case '4':
					time.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
					break;
				case '五':
				case '5':
					time.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
					break;
				case '六':
				case '6':
					time.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
					break;
				case '天':
				case '日':
				case '7':
					time.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
					break;
				}
				setWeek = true;
				setSureDate = true;
				year = time.get(Calendar.YEAR);
				month = time.get(Calendar.MONTH);
				day = time.get(Calendar.DATE);
			}
		}
		// 确定小时
		MC = tp.getTimeMatcher();
		if (MC.find()) {
			String[] IKTime = toIKMode(MC.group());
			if (IKTime[0].charAt(IKTime[0].length() - 1) == '点'
					|| IKTime[0].charAt(IKTime[0].length() - 1) == '时') {
				if (IKTime[0].length() == 2)// x点只有2个字符 无法确定是否为24时制
					hour = Integer.valueOf(IKTime[0].substring(0, 1));
				else if (Integer.valueOf(IKTime[0].substring(0, 2)) < 12)
					hour = Integer.valueOf(IKTime[0].substring(0, 2));
				else {// 一定表示24时制
					hour = Integer.valueOf(IKTime[0].substring(0, 2));
					setAPM = 1;
				}

			} else {
				if (IKTime[0].length() == 1)
					hour = Integer.valueOf(IKTime[0].substring(0, 1));
				else if (Integer.valueOf(IKTime[0].substring(0, 2)) < 12)
					hour = Integer.valueOf(IKTime[0].substring(0, 2));
				else {// 一定表示24时制
					hour = Integer.valueOf(IKTime[0].substring(0, 2));
					setAPM = 1;
				}
			}
			// 确定分钟
			if (IKTime.length == 1)
				minute = 0;
			else if (IKTime[1].charAt(IKTime[1].length() - 1) == '分') {
				if (IKTime[1].length() == 2)
					minute = Integer.valueOf(IKTime[1].substring(0, 1));
				else
					minute = Integer.valueOf(IKTime[1].substring(0, 2));
			} else if (IKTime[1].charAt(IKTime[1].length() - 1) == '刻') {
				switch (IKTime[1].charAt(0)) {
				case '一':
				case '1':
					minute = 15;
					break;
				case '二':
				case '2':
					minute = 30;
					break;
				case '三':
				case '3':
					minute = 45;
					break;
				}
			} else if (IKTime.length == 2 && IKTime[1].charAt(0) == '半')
				minute = 30;
			else
				minute = Integer.valueOf(IKTime[1]);
		}
		// 处理年月日
		if (year > 1970) {
			time.set(Calendar.YEAR, year);
			if (month >= 0 && month <= 11) {
				time.set(Calendar.MONTH, month);
				if (day >= 1 && day <= getDayOfMonth(year, month))
					time.set(Calendar.DATE, day);
			}
		}

		// 如果不等于原时间且hour>12证明不需要改时制
		MC = tp.getTSMatcher();
		if (hour != time.get(Calendar.HOUR_OF_DAY) && setAPM == -1 && MC.find()) {
			// 校准时制
			if ((MC.group().charAt(MC.start() - 1) >= '0' && MC.group().charAt(
					MC.start() - 1) <= '9')
					&& MC.group().charAt(MC.end()) >= '0'
					&& MC.group().charAt(MC.end()) <= '9') {
				if (MC.group().charAt(0) == 'P' || MC.group().charAt(0) == 'p')
					time.set(Calendar.AM_PM, Calendar.PM);
				else
					time.set(Calendar.AM_PM, Calendar.AM);
			}
		}
		// 处理小时分钟
		if (hour >= 0 && hour <= 24) {
			time.set(Calendar.HOUR_OF_DAY, hour);
			if (minute >= 0 && minute <= 60)
				time.set(Calendar.MINUTE, minute);
		}
		time.get(Calendar.HOUR_OF_DAY);
		TSFix(time, msg);// 早上等
		// 校准日期
		if (setSureDate == false)// 如果未定确定一个确切的日期则匹配明天等
			DateFix(time, msg);// 明天等
		return time;
	}

	private String[] toIKMode(String str) {
		for (int i = 1; i < str.length(); i++) {
			if (str.charAt(i) == ':' || str.charAt(i) == '：') {
				String[] IKMode = new String[2];
				IKMode[0] = str.substring(0, i);
				IKMode[1] = str.substring(i + 1, str.length());
				return IKMode;
			} else if (str.charAt(i) == '点' || str.charAt(i) == '时') {
				if (i + 1 == str.length()) {
					String[] IKMode = new String[1];
					IKMode[0] = str.substring(0, i + 1);
					return IKMode;
				} else {
					String[] IKMode = new String[2];
					IKMode[0] = str.substring(0, i + 1);
					IKMode[1] = str.substring(i + 1, str.length());
					return IKMode;
				}
			}
		}
		return null;
	}

	private void TSFix(Calendar time, String msg) {
		// for (int i = 0; i < text.length; i++) {
		// if (text[i].compareTo("上午") == 0 || text[i].compareTo("清晨") == 0
		// || text[i].compareTo("早晨") == 0
		// || text[i].compareTo("早上") == 0
		// || text[i].compareTo("今天上午") == 0) {
		// time.set(Calendar.AM_PM, Calendar.AM);
		// setAPM = 0;
		// } else if (text[i].compareTo("中午") == 0
		// || text[i].compareTo("下午") == 0
		// || text[i].compareTo("晚上") == 0
		// || text[i].compareTo("今晚") == 0
		// || text[i].compareTo("傍晚") == 0
		// || text[i].compareTo("半夜") == 0
		// || text[i].compareTo("午夜") == 0
		// || text[i].compareTo("今天中午") == 0
		// || text[i].compareTo("今天下午") == 0) {
		// time.set(Calendar.AM_PM, Calendar.PM);
		// setAPM = 1;
		// } else if (text[i].compareTo("凌晨") == 0) {// 特殊
		// time.set(Calendar.AM_PM, Calendar.AM);
		// time.set(Calendar.DAY_OF_YEAR,
		// time.get(Calendar.DAY_OF_YEAR) + 1);
		// setAPM = 0;
		// } else if (text[i].compareTo("明早") == 0) {// 特殊
		// time.set(Calendar.AM_PM, Calendar.PM);
		// time.set(Calendar.DAY_OF_YEAR,
		// time.get(Calendar.DAY_OF_YEAR) + 1);
		// setAPM = 0;
		// } else if (text[i].compareTo("明晚") == 0) {// 特殊
		// time.set(Calendar.AM_PM, Calendar.PM);
		// time.set(Calendar.DAY_OF_YEAR,
		// time.get(Calendar.DAY_OF_YEAR) + 1);
		// setAPM = 1;
		// }
		// }
		if (msg.contains("上午") || msg.contains("清晨") || msg.contains("早晨")
				|| msg.contains("早上")) {
			time.set(Calendar.AM_PM, Calendar.AM);
			setAPM = 0;
		} else if (msg.contains("中午") || msg.contains("下午")
				|| msg.contains("晚上") || msg.contains("今晚")
				|| msg.contains("傍晚") || msg.contains("半夜")
				|| msg.contains("午夜")) {
			time.set(Calendar.AM_PM, Calendar.PM);
			setAPM = 1;
		} else if (msg.contains("凌晨")) {
			time.set(Calendar.AM_PM, Calendar.AM);
			time.set(Calendar.DAY_OF_YEAR, time.get(Calendar.DAY_OF_YEAR) + 1);
			setAPM = 0;
		} else if (msg.contains("明早")) {
			time.set(Calendar.DAY_OF_YEAR, time.get(Calendar.DAY_OF_YEAR) + 1);
			setAPM = 0;
		} else if (msg.contains("明晚")) {
			time.set(Calendar.DAY_OF_YEAR, time.get(Calendar.DAY_OF_YEAR) + 1);
			setAPM = 1;
		}
	}

	private void DateFix(Calendar time, String msg) {
		// for (int i = 0; i < text.length; i++) {
		// if (text[i].compareTo("明天") == 0) {
		// time.set(Calendar.DATE, time.get(Calendar.DATE) + 1);
		// break;
		// } else if (text[i].compareTo("后天") == 0) {
		// time.set(Calendar.DATE, time.get(Calendar.DATE) + 2);
		// break;
		// } else if (text[i].compareTo("大后天") == 0) {
		// time.set(Calendar.DATE, time.get(Calendar.DATE) + 3);
		// break;
		// }
		// }
		if (msg.contains("明天")) {
			time.set(Calendar.DATE, time.get(Calendar.DATE) + 1);
		} else if (msg.contains("后天")) {
			time.set(Calendar.DATE, time.get(Calendar.DATE) + 2);
		} else if (msg.contains("大后天")) {
			time.set(Calendar.DATE, time.get(Calendar.DATE) + 3);
		}
	}

	private void dropAllDate() {
		Pattern Year = Pattern.compile("\\d{2,4}[年\\.\\/\\-]");// xx-xxxx年
		Pattern Month = Pattern.compile("\\d{1,2}[月\\.\\/\\-]");// x-xx月
		Pattern Day = Pattern.compile("\\d{1,2}[日号]");// x-xx日
		Pattern Week = Pattern.compile("(星期|礼拜|周)[一二三四五六日天1-7]");// 星期x
		Pattern NextWeek = Pattern.compile("下(星期|礼拜|周)[一二三四五六日天1-7]");// 下星期x
		// Pattern TS = Pattern.compile("[AaPp]\\.?[Mm]\\.?");// am/pm
		Pattern Time = Pattern
				.compile("\\d{1,2}(([：:]\\d{1,2})|([点](\\d{1,2}分|半|[123一二三]刻)?))");// 精确时间
		Pattern APM = Pattern.compile("(上午|中午|下午|清晨|早晨|晚上|傍晚|半夜|午夜|凌晨)");// am/pm
		Pattern Date = Pattern.compile("(明天|后天|大后天)");
		deleteDateWord(Year);
		deleteDateWord(Month);
		deleteDateWord(Day);
		deleteDateWord(Week);
		deleteDateWord(NextWeek);
		// deleteDateWord(TS);
		deleteDateWord(Time);
		deleteDateWord(APM);
		deleteDateWord(Date);
	}

	private int getDayOfMonth(int year, int month) {
		switch (month) {
		case 0:
			return 31;
		case 1:
			if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
				return 29;
			else
				return 28;
		case 2:
			return 31;
		case 3:
			return 30;
		case 4:
			return 31;
		case 5:
			return 30;
		case 6:
			return 31;
		case 7:
			return 30;
		case 8:
			return 31;
		case 9:
			return 30;
		case 10:
			return 30;
		case 11:
			return 31;
		default:
			return 0;
		}
	}

	private void deleteDateWord(Pattern p) {
		Matcher MC = p.matcher(NoDateMsg);
		StringBuffer strb = new StringBuffer(NoDateMsg);
		while (MC.find()) {
			strb.replace(MC.start(), MC.end(), "");
			NoDateMsg = strb.toString();
			MC = p.matcher(NoDateMsg);
		}
	}

	private TimePattern tp;
	private String Msg;
	private String NoDateMsg;
	private String[] text;// 记录分词后的结果
	private ArrayList<Calendar> timeList;// 记录时间
	// private String Location;// 记录地点
	private int setAPM = -1;// 短信中是否有确定上下午-1表示未定 0表示早上 1表示下午
	private boolean chineseNumber;
	private boolean hasTime = false;// 是否有时间
}
// ////////////////////////////////////////////
// programmer:MK email:lin199231@gmail.com